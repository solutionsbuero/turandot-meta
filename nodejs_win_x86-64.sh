# set working Directory
cd "${0%/*}"

#########################
# Move Python stuff
#########################

# mv turandot/dist/Turandot/* turandot-releases/windows_x86-64/


#########################
# Copy citeproc
#########################

rm -rf turandot-front/citeproc-js-server
cp -r citeproc-js-server turandot-front/


#########################
# Build citeproc (after copy)
#########################

cd turandot-front/citeproc-js-server
rm -rf git*
npm install .
pkg --target node8-win-x64 --output ./citeproc.exe .
rm -rf node_modules


#########################
# electron-packager
#########################

cd ..
npm install
electron-packager --overwrite --platform win32 --arch x64 --icon=./icons/Icon.ico --out ../turandot-releases .


#########################
# Package stuff
#########################

cd ../turandot-releases
zip -r turandot-win32-x64.zip turandot-win32-x64


#########################
# Cleanup
#########################

cd ../turandot-front
rm -rf citeproc-js-server
rm -rf turandot-srv
rm -rf node_modules
