#!/usr/bin/env bash

# set working Directory
cd "${0%/*}"

# Cleanup
rm -rf turandot-releases/linux_x86-64/*
rm turandot-releases/turandot_linux_x86-64.tar.gz

#########################
# PyInstaller
#########################

cd turandot
if [ -d "venv" ]
then
    echo "Found existing venv, skipping creation"
else
    python3 -m venv venv
fi

# Install Turandot in venv
./venv/bin/pip3 install -r requirements.txt .

# Do cleanup
rm -rf dist build

# Build executable
./venv/bin/pyinstaller Turandot_linux.spec


retVal=$?
if [ $retVal -ne 0 ]
then
    echo "PyInstaller failed with exit code $retVal"
else
    echo "PyInstaller finished successfully"
fi


#########################
# Move Python stuff
#########################

cd ..
mkdir turandot-front/turandot-srv
mv turandot/dist/turandot-srv/* turandot-front/turandot-srv/


#########################
# Copy citeproc
#########################

cp -r citeproc-js-server turandot-front/


#########################
# Build citeproc
#########################

cd turandot-front/citeproc-js-server
rm -rf .git*
npm install .
pkg --target node8-linux-x64  --output ./citeproc .
rm -rf node_modules


#########################
# electron-packager
#########################

cd ..
npm install
rm -rf ../turandot-releases/turandot-linux-x64
electron-packager --overwrite --platform linux --arch x64 --icon=./icons/linux/1024.png --out ../turandot-releases .


#########################
# Package stuff
#########################

cd ../turandot-releases
rm turandot_linux_x64.tar.gz
tar -czvf turandot-linux-x64.tar.gz turandot-linux-x64


#########################
# Cleanup
#########################

cd ../turandot-front
rm -rf citeproc-js-server
rm -rf turandot-srv
rm -rf node_modules
