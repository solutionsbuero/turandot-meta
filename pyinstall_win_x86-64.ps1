# Set working directoy
$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
# cd $dir
Push-Location $dir # Temporarily change working directory
Write-host "Current working directory:"
Get-Location

# Create venv, install dependencies
cd .\turandot
if ( -not (Test-Path -LiteralPath '.\wvenv' -PathType Container) ) {
    python3 -m venv wvenv
}
else {"venv found, skipping creation"}

.\wvenv\Scripts\Activate.ps1

# pip install stuff
pip3 install -r .\requirements.txt .

# launch PyInstaller
pyinstaller --noconfirm --windowed --noconsole .\Turandot_windows.spec

deactivate

New-Item -Path "E:\turandot-front\" -Name "turandot-srv" -ItemType "directory"
Move-Item -force -path .\dist\turandot-srv\* -destination E:\turandot-front\turandot-srv\

Pop-Location # Reset working directory
